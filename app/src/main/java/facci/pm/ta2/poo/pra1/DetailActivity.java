package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.graphics.drawable.Icon;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.FindCallback;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    TextView description, title, price;
    ImageView thumbnail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);



        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6

        //Se recibe el intent enviado desde ResultActivity con su extra
        String object_id = getIntent().getStringExtra("object_id");

        TextView description = (TextView) findViewById(R.id.description);
        description.setMovementMethod(LinkMovementMethod.getInstance());



        DataQuery query = DataQuery.get("item");
        query.getInBackground(object_id, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e == null){

                    //Se accede a las propiedas de los siguientes objetos
                    TextView description = (TextView) findViewById(R.id.description);
                    TextView price = (TextView) findViewById(R.id.price);
                    TextView title = (TextView) findViewById(R.id.title);
                    ImageView thumbnail = (ImageView) findViewById(R.id.thumbnail);

                    //Le asignamos a cada objeto el texto debido por medio del objeto
                    //En el caso de la imagen se le asigna la imagen correspondiente
                    description.setText((String) object.get("description"));
                    price.setText((String) object.get("price")+"\u0024");
                    title.setText((String) object.get("name"));
                    thumbnail.setImageBitmap((Bitmap) object.get("image"));



                } else {
                    //Error
                }
            }
        });



        // FIN - CODE6

    }

}
